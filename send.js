const nodemailer = require('nodemailer');

async function sendMessage(message) {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "smtp.yandex.ru",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.IMAP_LOGIN, // generated ethereal user
            pass: process.env.IMAP_PSW // generated ethereal password
        }
    });

    // send mail with defined transport object
    //   let info =
    await transporter.sendMail({
        from: process.env.IMAP_LOGIN, // sender address
        to: message.to, // list of receivers
        subject: message.subject, // Subject line
        text: message.text, // plain text body
        attachments: message.attachments ? message.attachments : []
    });
}

module.exports.sendMessage = sendMessage;