const test = require('./simple');
const send = require('./send');

module.exports = function (app) {
    app.get('/', (req, res) => {
        res.send('Doge server here');
    });

    app.post('/get-letters', (req, res) => {

        // console.log(test.getInboxMessages());

        if (
            req.body.login == process.env.IMAP_LOGIN &&
            req.body.psw == process.env.IMAP_PSW
        ) {
            res.send(test.getInboxMessages());
        } else {
            res.send('Nope');
        }
    });

    app.post('/send-message', (req, res) => {

        console.log(req.body);

        if (
            req.body.login == process.env.IMAP_LOGIN &&
            req.body.psw == process.env.IMAP_PSW
        ) {
            // res.send(test.getInboxMessages());
            send.sendMessage(req.body.message);
            res.sendStatus(200);
        } else {
            res.send('Nope');
        }
    });

    app.post('/delete-message', (req, res) => {

        console.log(req.body);

        if (
            req.body.login == process.env.IMAP_LOGIN &&
            req.body.psw == process.env.IMAP_PSW
        ) {
            // res.send(test.getInboxMessages());
            test.deleteMessage(req.body.id);
            res.sendStatus(200);
        } else {
            res.send('Nope');
        }
    });
}