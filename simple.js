var imaps = require('imap-simple');
require('dotenv').config();

let inboxMessages = [];
module.exports.getInboxMessages = () => inboxMessages;
module.exports.deleteMessage = deleteMessage;

var config = {
    imap: {
        user: process.env.IMAP_LOGIN,
        password: `${process.env.IMAP_PSW}`,
        host: 'imap.yandex.ru',
        port: 993,
        tls: true,
        authTimeout: 3000
    }
};


imaps.connect(config).then(function (connection) {
    return connection.openBox('INBOX').then(function () {

        console.log('Getting indox messages...');

        var searchCriteria = ['UNDELETED'];

        var fetchOptions = {
            bodies: ['HEADER', 'TEXT'],
            markSeen: false,
            struct: true
        };

        return connection.search(searchCriteria, fetchOptions)
            .then(async function (messages) {

                let result = [];

                result = await messages.map(async function (message) {
                    let parts = imaps.getParts(message.attributes.struct);

                    let attachments = parts.filter(function (part) {
                        return part.disposition && part.disposition.type.toUpperCase() === 'ATTACHMENT';
                    }).map(function (part) {
                        return connection.getPartData(message, part)
                            .then(function (partData) {
                                return {
                                    filename: part.disposition.params.filename,
                                    data: partData.toString('base64')
                                };
                            });
                    });

                    let body = parts.filter(function (part) {
                        return part.type.toUpperCase() === 'TEXT';
                    }).map(function (part) {
                        return connection.getPartData(message, part)
                            .then(function (partData) {
                                return {
                                    type: part.subtype,
                                    data: partData.toString('utf8')
                                };
                            });
                    });

                    let headers = message.parts.find(part => part.which == "HEADER").body;

                    return ({
                        id: message.attributes.uid,
                        from: headers.from,
                        to: headers.to,
                        date: headers.date,
                        subject: headers.subject,
                        body: await Promise.all(body),
                        attachments: await Promise.all(attachments)
                    });
                });

                return Promise.all(result);
            }).then(function (messages) {
                console.log('Complete! ', messages.length, ' messages.');
                inboxMessages = messages;
                connection.end();
            });
    });

});

function deleteMessage(id) {
    imaps.connect(config).then(function (connection) {
        return connection.openBox('INBOX').then(function () {
            console.log('Getting indox messages...');
            var searchCriteria = [['UID', id]];

            var fetchOptions = {
                bodies: ['HEADER'],
                markSeen: false,
                struct: true
            };

            return connection.search(searchCriteria, fetchOptions)
                .then(function (messages) {
                    messages.forEach(async function (message) {
                        console.log(message.attributes.uid);
                        Promise.resolve(connection.addFlags(message.attributes.uid, 'DELETED'))
                    });

                    connection.end();
                });
        });

    });
}