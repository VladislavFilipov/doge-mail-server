require('dotenv').config();
const Imap = require('imap');
const inspect = require('util').inspect;
const MailParser = require('mailparser').MailParser;
// const simpleParser = require('mailparser').simpleParser;
const Iconv = require('iconv').Iconv;

let lettersList = [];
module.exports.getLettersList = () => lettersList;

const imap = new Imap({
    user: process.env.IMAP_LOGIN,
    password: `${process.env.IMAP_PSW}`,
    host: 'imap.yandex.ru',
    port: 993,
    tls: true,
    attachments: true
});

const openInbox = cb => {
    imap.openBox('INBOX', true, cb);
}

imap.once('ready', getDataFromImapServer);
imap.once('mail', () => console.log('New Message'));

imap.once('error', err => {

    console.log(err);

});

imap.once('end', () => {

    console.log('Connection ended');

});

imap.connect();

function getFetchRange(num, total) {
    if (num)
        return total - num;
    else
        return 1;
}

function getDataFromImapServer(num) {

    openInbox((err, box) => {

        if (err) throw err;

        console.log(box.messages.total + ':' + getFetchRange(num, box.messages.total))

        // const fetchStream = imap.seq.fetch(box.messages.total + ':' + getFetchRange(num, box.messages.total), {
        const fetchStream = imap.seq.fetch((box.messages.total - 7) + ':' + (box.messages.total - 7), {

            bodies: ['HEADER.FIELDS (FROM TO SUBJECT DATE)', 'TEXT'],
            struct: true

        });

        fetchStream.on('message', (msg, seqno) => {

            console.log('Message #%d', seqno);



            const prefix = '(#' + seqno + ') ';

            // let temp = { id: seqno };

            msg.on('body', (stream, info) => {

                let buffer = '';

                let parser = new MailParser({ Iconv });

                parser.on('headers', headers => {
                    console.log("HEADERS", headers);
                });

                parser.on('data', data => {

                    console.log("DATA", data);
                    // if (data.type === 'attachment') {
                    //     console.log(data.filename);
                    //     data.content.pipe(process.stdout);
                    //     data.content.on('end', () => data.release());
                    // }

                    // if (data.type === 'text') {
                    //     console.log(data.html);
                    // }
                });

                // parser.on('end', mailObj => {
                //     // console.log(mailObj.subject, mailObj.from, mailObj.attachments);
                //     console.log(mailObj);
                // })



                stream.on('data', (chunk) => {
                    buffer += chunk.toString('utf8');
                    // parser.write(chunk.toString('utf8'));
                });

                stream.on('end', () => {
                    parser.write(buffer);
                    parser.end();
                    // console.log(prefix + 'Parsed header: %s', inspect(Imap.parseHeader(buffer)));
                    // console.log(buffer);

                    // console.log('END', buffer)

                    // simpleParser(buffer, { skipTextToHtml: true }, (err, parsed) => {
                    //     console.log(parsed)
                    //     if (info.which != 'TEXT') {
                    //         // console.log("header")
                    //         temp.date = parsed.date;
                    //         temp.subject = parsed.subject;
                    //         temp.from = parsed.from.value;
                    //         temp.to = parsed.to ? parsed.to.value : '';
                    //     } else {
                    //         // console.log("text")
                    //         temp.content = parsed.text;
                    //         // console.log(temp);
                    //         lettersList.push(temp);
                    //     }

                    // });


                    // parser.feed(buffer)

                    // lettersList.push(buffer);
                });

            });

            msg.once('attributes', attrs => {

                // console.log(attrs);
                console.log(prefix + 'Attributes: %s', inspect(attrs, false, 8));

            });

            msg.once('end', () => {


                // console.log(prefix + 'Finished');

            });

        });

        fetchStream.once('error', err => {

            // console.log('Fetch error: ' + err);

        });

        fetchStream.once('end', () => {

            // console.log('Done fetching all messages!');

            imap.end();

        });

    });

};