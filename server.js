"use strict";
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();
// const fs = require('fs');


const app = express();
const port = 1488;

app.use(bodyParser.json({ limit: '20mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }));

app.use(cors({ origin: '*' }));
app.use('/static', express.static('static'));

// ----------------------------------------------------------

const requests = require('./requests');
requests(app);

// ----------------------------------------------------------

app.listen(port, () => {
    console.log('Running on port ' + port);
});



